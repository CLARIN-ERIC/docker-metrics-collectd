= Metrics collectd image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker image running a https://collectd.org/[collectd] daemon.

## Exposed ports

The following ports are exposed by this image:

.Exposed ports
[cols=",,"]
|====
| Port  | Type | Description
| 25826 | udp  | collectd port
|====

## Volumes

.Exposed volumes
[cols=","]
|====
| Path                  | Description
| /usr/share/collectd/  | collectd types database
|====

